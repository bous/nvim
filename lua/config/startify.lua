vim.g.startify_session_autoload = 1
vim.g.startify_fortune_use_unicode = 1
vim.g.startify_files_number = 5

vim.g.startify_lists = {
    { type = "sessions",  header = {"    Sessions"       }},
    { type = "bookmarks", header = {"    Bookmarks"      }},
    { type = "files",     header = {"    Recently edited"}},
    { type = "commands",  header = {"    Commands"       }}
}

vim.g.startify_bookmarks = {
    { o = "~/Code/NeuralVocoder/"  },
    { a = "~/Code/AS/as_pysrc/"    },
    { c = "~/Code/"                },
    { t = "~/Publications/Thesis/" },
    { l = "~/.config/nvim/lua/"    },
    { r = "~/.bashrc"              }
}

vim.g.startify_commands = {
    { h = "h ref" },
    { g = "VimGameSnake" },
    { T = "StartTidal" }
}
