local map = vim.api.nvim_set_keymap

vim.g.glow_binary_path = vim.env.HOME .. "/bin"
vim.g.glow_border = "rounded"
vim.g.glow_style = "dark"

map("n", "<leader>p", "<CMD>Glow<CR>", {noremap = true})

