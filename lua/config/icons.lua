vim.o.guifont = "Fira Code Retina Nerd Font Complete"
vim.g.webdevicons_enable_startify = 1
vim.g.webdevicons_enable_nerdtree = 1
vim.g.webdevicons_enable_airline  = 1
