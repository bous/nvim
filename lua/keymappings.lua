---------------- Normal Mode --------------------------------------------------
-------- Function keys

---- F1-4: General functions
-- <F1> is help
-- <F2> Toggle history tree
vim.keymap.set("n", "<F2>", "<CMD>UndotreeToggle<CR>")
-- <F3> Toggle file tree
--vim.keymap.set("n", "<F3>", "<CMD>NERDTreeToggle<CR>")
vim.keymap.set("n", "<F3>", "<CMD>NvimTreeFocus<CR>")
vim.keymap.set("n", "<C-F3>", "<CMD>NvimTreeToggle<CR>")
-- <F4> Symbols outline
vim.keymap.set("n", "<F4>", "<CMD>SymbolsOutline<CR>")

---- F5-8: execute files
-- <F5> Run current file
vim.keymap.set( "n", "<F5>", function ()
    vim.cmd "wa"
    vim.cmd [[!./%]]
end)
-- Hit <F5> twice to make it executable before running
vim.keymap.set("n", "<F5><F5>", function ()
    vim.cmd "wa"
    vim.cmd [[!chmod u+x %]]
    vim.cmd [[!./%]]
end)
-- <F6> Run the file named ./main
vim.keymap.set("n", "<F6>", function ()
    vim.cmd "wa"
    vim.cmd [[!./main]]
end)

---- F9-12: Make
-- <F9> Quick build (`make`)
--vim.keymap.set("n", "<F9>", function ()
--    vim.cmd "wa"
--    require("toggleterm").exec("make")
--end)
--
---- <F10> Full build (`make all`)
--vim.keymap.set("n", "<F10>", function ()
--    vim.cmd "wa"
--    require("toggleterm").exec("make all")
--end)
--
---- <F11> Cleanup (`make clean`)
--vim.keymap.set("n", "<F11>", function ()
--    vim.cmd "wa"
--    require("toggleterm").exec("make clean")
--end)
--
---- <F12> Release build (`make release`)
--vim.keymap.set("n", "<F12>", function ()
--    vim.cmd "wa"
--    require("toggleterm").exec("make release")
--end)

-------- Pasting
vim.keymap.set("n", "Y", "y$")              -- yank eveything after the cursor
vim.keymap.set({"n", "v"}, "<C-C>", '"+y')  -- copy into system buffer

-------- Arrow Keys
vim.keymap.set("n", "<Up>", ":<Up>")
vim.keymap.set("n", "<Down>", "<NOP>")
vim.keymap.set("n", "<Left>", "<NOP>")
vim.keymap.set("n", "<Right>", "<NOP>")

-------- Misc
-- Toggle comment and go to next line
vim.keymap.set("n", "<C-/>", "<Leader>c j", {noremap = false})
vim.keymap.set("v", "<C-/>", "<Leader>c ", {noremap = false})

-- Go back in buffers
vim.keymap.set("n", "]b", "<Plug>(buf-surf-forward)")
vim.keymap.set("n", "[b", "<Plug>(buf-surf-back)")

-- Jump around in snippets
local ls = require "luasnip"
vim.keymap.set("i", "<PageDOWN>", function() ls.jump(1) end)
vim.keymap.set("s", "<PageDOWN>", function() ls.jump(1) end)
vim.keymap.set("i", "<PageUP>", function() ls.jump(-1) end)
vim.keymap.set("s", "<PageUP>", function() ls.jump(-1) end)

---------------- Insert Mode --------------------------------------------------
vim.keymap.set("i", "<C-L>", "<DEL>")       -- Delete on ^L
vim.keymap.set("i", "<C-J>", "<C-M>")       -- New line on ^J
vim.keymap.set("i", "<C-K>", "<Esc>O")      -- New line above on ^K
vim.keymap.set("i", "<PageDOWN>", "<Esc>o") -- New line below
-- No sad faces when accidetally hitting ^U
vim.keymap.set("i", "<C-U>", "<C-K>")
vim.keymap.set("i", "<C-S>", "<C-G>u<Esc>[s1z=`]a<C-G>u")

vim.keymap.set("i", "<C-@>", "<C-x><C-n>")
