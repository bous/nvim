---------------- PYTHON -------------------------------------------------------

local py = vim.api.nvim_create_augroup("pyformat", {})
vim.api.nvim_create_autocmd(
    "BufWrite",
    {
        group = py,
        pattern = "*.py",
        callback = function ()
            vim.cmd "Isort"
            vim.cmd "Black"
        end
    }
)

vim.keymap.set("n", "<Leader>mp", "<CMD>!mypy %<CR>")

---------------- LATEX --------------------------------------------------------

vim.api.nvim_create_autocmd(
    {"BufNewFile", "BufRead"},
    {
        pattern = {"*.tex", "*.tikz", "*.sty", "*.cls"},
        callback = function ()
            vim.bo.filetype = "tex"
            vim.wo.spell = true
            vim.bo.spelllang = "en_gb"
            vim.bo.shiftwidth = 2
            vim.bo.softtabstop = 2
            vim.wo.wrap = true
            vim.wo.linebreak = true
            vim.wo.colorcolumn=""
        end
    }
)

---------------- HASKELL ------------------------------------------------------

vim.api.nvim_create_autocmd(
    {"BufNewFile", "BufRead"},
    {
        pattern = {"*.hs", "*.tidal"},
        callback = function ()
            vim.bo.shiftwidth = 2
            vim.bo.softtabstop = 2
        end
    }
)

---------------- TIDAL --------------------------------------------------------

require "lang.tidal"
--vim.g.tidal_target = "terminal"
