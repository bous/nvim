local map = vim.api.nvim_set_keymap

local module = {}

vim.g.tidal_target = "terminal"

local _get_today = function ()
    local date = os.date("*t")
    return string.format("%d-%d-%d", date.year, date.month, date.day)
end

local function scnvim_run(file)
    local cmd = string.format("\"%s\".load", file)
    require("scnvim").send(cmd)
end

module.start_midi = function()
    local startup_midi_file = "/home/bous/Code/Music/Tidal/super-collider/midi.scd"
    scnvim_run(startup_midi_file)
end

module.start_tidal = function()
    local date = _get_today()
    local startup_file = "/home/bous/Code/Music/Tidal/super-collider/startup.scd"
    vim.cmd("cd /home/bous/Code/Music/Tidal")
    vim.cmd(string.format("e %s", startup_file))
    require("scnvim").start()
    scnvim_run(startup_file)
    vim.cmd(string.format("e practice/%s.tidal", date))
end

module.insert_template = function(name)
    vim.cmd(string.format("r templates/%s.tidal", name))
end

module.edit_template = function(name)
    vim.cmd(string.format("sp templates/%s.tidal", name))
end

-- TT for tidal template
vim.cmd([[command! -nargs=1 TT lua require("lang.tidal").insert_template("<args>") ]])
vim.cmd([[command! -nargs=1 TTed lua require("lang.tidal").edit_template("<args>") ]])

vim.cmd([[command! StartTidal lua require("lang.tidal").start_tidal() ]])
vim.cmd([[command! TidalStartMidi lua require("lang.tidal").start_midi() ]])

-- Keep undo history in file for *.tidal files
vim.cmd [[au BufWritePre *.tidal setlocal undofile]]

map("n", "<leader>hh", "<CMD>TidalHush<CR>", {noremap=true})

return module
