vim.o.encoding = "utf-8"

vim.o.hidden = true
vim.o.wildmenu = true
vim.o.showcmd = true

vim.o.incsearch = true
vim.o.smartcase = true
vim.bo.autoindent = true

vim.wo.colorcolumn = "80"

vim.o.ruler = true
vim.o.laststatus = 2

vim.o.confirm = true
vim.o.visualbell = true

vim.o.mouse = "nv"
vim.o.cmdheight = 2

vim.bo.shiftwidth = 4
vim.bo.tabstop = 4
vim.bo.softtabstop = 4
vim.o.expandtab = true

vim.o.scrolloff = 5
vim.o.backspace = "indent,eol,start"

vim.wo.list = true
vim.o.showbreak = "↪ "
vim.o.listchars = "tab:» ,trail:˚,nbsp:␣,extends:›,precedes:‹"

vim.o.clipboard = "unnamedplus"
vim.cmd 'au TextYankPost * lua vim.highlight.on_yank {on_visual = false}'

vim.wo.number = true
vim.wo.relativenumber = true

local nt = vim.api.nvim_create_augroup("numbertoggle", {})
vim.api.nvim_create_autocmd(
    {"InsertLeave", "WinEnter"},
    {
        group = nt,
        pattern = "*",
        callback = function (args)
            if vim.fn.buflisted(args.buf) == 1 then
                vim.wo.relativenumber = true
            end
        end
    }
)
vim.api.nvim_create_autocmd(
    {"WinLeave", "InsertEnter"},
    {
        group = nt,
        pattern = "*",
        callback = function () vim.wo.relativenumber = false end
    }
)

local cl = vim.api.nvim_create_augroup("cursorline", {})
vim.api.nvim_create_autocmd(
    "InsertLeave",
    {
        group = cl,
        pattern = "*",
        callback = function () vim.wo.cursorline = false end
    }
)
vim.api.nvim_create_autocmd(
    "InsertEnter",
    {
        group = cl,
        pattern = "*",
        callback = function () vim.wo.cursorline = true end
    }
)
