return require("packer").startup(function()
    use { "wbthomason/packer.nvim", opt = true}  -- the package manager

    use { "mhinz/vim-startify" }                 -- startup screen

    -- language server
    use { "williamboman/mason.nvim" }            -- Install things
    use { "williamboman/mason-lspconfig.nvim" }
    use { "neovim/nvim-lspconfig"}               -- language server
    use { "hrsh7th/cmp-nvim-lsp" }
    use { "hrsh7th/cmp-buffer" }
    use { "hrsh7th/cmp-path" }
    use { "hrsh7th/cmp-cmdline" }
    use { "hrsh7th/nvim-cmp" }
    use { "jose-elias-alvarez/null-ls.nvim" }    -- Linters (mypy)

    -- snippets
    use { "saadparwaiz1/cmp_luasnip" }
    use { "L3MON4D3/LuaSnip" }
    use "rafamadriz/friendly-snippets"

    -- language extensions
    use {                                        -- Super collider
        "davidgranstrom/scnvim",
        run = function () vim.fn["scnvim#install"]() end
    }
    use { "tidalcycles/vim-tidal" }              -- For tidal cycles
    use { "psf/black" }                          -- formater for python
    use { "fisadev/vim-isort" }                  -- sort imports for python
    use { "lervag/vimtex" }                      -- for latex
    use { "kaarmu/typst.vim", ft = {"typst"} }   -- for typst

    -- additional functionality
    use {
      'kyazdani42/nvim-tree.lua',
      requires = { 'kyazdani42/nvim-web-devicons' },
      config = function() require("nvim-tree").setup() end,
      tag = 'nightly' -- optional, updated every week. (see issue #1193)
    }
    use { "scrooloose/nerdcommenter" }           -- facilitate commenting
    use { "peterrincker/vim-argumentative" }     -- Navigate and swap arguments
    use {                                        -- Surround objects by braces
      "kylechui/nvim-surround",
      config = function() require("nvim-surround").setup({}) end
    }
    use { "mbbill/undotree" }                    -- command history tree
    use { "simrat39/symbols-outline.nvim" }      -- tree view of symbols
    use { "tpope/vim-fugitive" }                 -- git integration
    use { "tpope/vim-sleuth" }                   -- automatic shiftwidth
    use { "iamcco/markdown-preview.nvim" }       -- Render markdown in browser
    use { "ellisonleao/glow.nvim" }              -- Render markdown in buffer
    use {                                        -- Fuzzy finder
        "nvim-telescope/telescope.nvim",
        requires = {
            {"nvim-lua/popup.nvim"},
            {"nvim-lua/plenary.nvim"},
            {"BurntSushi/ripgrep"},
            {"sharkdp/fd"}
        }
    }
    use {                                        -- Generate closing braces
        "windwp/nvim-autopairs",
        config = function() require("nvim-autopairs").setup() end
    }
    use { "wellle/context.vim" }                 -- Show surrounding context
    use { "wellle/targets.vim" }                 -- Additional vim objects
    use {                                        -- popup window with options
        "folke/which-key.nvim",
        config = function() require("which-key").setup() end
    }
    use "nvim-treesitter/nvim-treesitter"
    use "ton/vim-bufsurf"

    -- Theming
    use { "vim-airline/vim-airline" }            -- status- and buffer line
    use { "vim-airline/vim-airline-themes" }     -- thmes for status line
    use { "gryf/wombat256grf" }                  -- colour scheme

    -- Misc
    use { "johngrib/vim-game-snake" }            -- include the snake game
    use {
        "glacambre/firenvim",
        run = function() vim.fn["firenvim#install"](0) end
    }
end)
