require("mason").setup()
require("mason-lspconfig").setup {
    ensure_installed = { "typst_lsp" },
    automatic_installation = false,
}
local nvim_lsp = require('lspconfig')

local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- Mappings
    local opts = { noremap=true, silent=true }
    buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
    buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
    buf_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
end

-- To get builtin LSP running, do something like:
-- NOTE: This replaces the calls where you would have before done `require('nvim_lsp').sumneko_lua.setup()`

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

--local sumneko_path = "/home/bous/Code/Misc/lua-language-server/"
--local sumneko_binary = sumneko_path .. "bin/Linux/lua-language-server"

--nvim_lsp.sumneko_lua.setup{
--    cmd = {sumneko_binary, "-E", sumneko_path .. "/main.lua"};
--    settings = {
--        Lua = {
--            version = "LuaJIT",
--            path = runtime_path,
--            diagnostics = {
--                globals = {"vim"}
--            },
--        },
--        workspace = {
--            library = vim.api.nvim_get_runtime_file("", true)
--        },
--        telemetry = { enable = false, },
--    };
--    on_attach = on_attach
--}
nvim_lsp.pyright.setup{ on_attach=on_attach }
nvim_lsp.hls.setup{
    on_attach = on_attach;
    filetypes = {"haskell", "lhaskell", "tidal"}
}
local capabilities = require("cmp_nvim_lsp").update_capabilities(
    vim.lsp.protocol.make_client_capabilities()
)
nvim_lsp.pyright.setup {
    on_attach=on_attach;
    capabilities=capabilities;
}

nvim_lsp.hls.setup{ on_attach=on_attach }
nvim_lsp.texlab.setup{
    filetypes = {"tex", "bib", "latex"};
    on_attach=on_attach
}
nvim_lsp.bashls.setup{ on_attach = on_attach }

nvim_lsp.clangd.setup {}

nvim_lsp.ltex.setup {
    on_attach=on_attach,
    filetypes = {"tex", "latex", "markdown", "typst"},
    settings={
        ltex={
            language="en-GB",
        }
    }
}

nvim_lsp.typst_lsp.setup {
    on_attach=on_attach,
    settings = {
        exportPdf = "onType"
    }
}
