vim.g.mapleader = ','

local fn = vim.fn
local execute = vim.api.nvim_command

require("settings")

local install_path = fn.stdpath("data").."/site/pack/packer/opt/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
    execute("!git clone https://github.com/wbthomason/packer.nvim "..install_path)
end
vim.cmd [[packadd packer.nvim]]
vim.api.nvim_create_autocmd("BufWritePost", {
    pattern = "plugins.lua",
    callback = function (args)
        dofile(args.file)
        local packer = require("packer")
        packer.compile()
        packer.install()
    end
})
--vim.cmd "autocmd BufWritePost plugins.lua PackerCompile"

require("plugins")
require("lsp")
require("lang")
require("keymappings")
require("config")

